import React from "react";
import ReactDOM from "react-dom";
import { createStore } from "redux";
import { Provider } from "react-redux";

import "Styles/Main.scss";
import App from "./App";

//initial state
const globalState = {
  favouritePokemon: [],
}

//reducer
const rootReducer = (state = globalState, action) => {
  switch (action.type) {
    case "SET_FAVOURITE_POKEMON":
      return {
        ...state,
        favouritePokemon: state.favouritePokemon.concat(
          action.newValue
        ),
      };
    default:
      break;
  }
  return state;
};

const store = createStore(rootReducer);

ReactDOM.render(
  <React.StrictMode>
    <Provider store = {storeRedux}>
      <App />
    </Provider>
  </React.StrictMode>,
  document.getElementById("root")
);
